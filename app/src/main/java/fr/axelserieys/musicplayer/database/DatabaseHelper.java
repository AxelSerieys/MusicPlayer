package fr.axelserieys.musicplayer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.models.Playlist;
import fr.axelserieys.musicplayer.models.Song;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper databaseHelper;

    private static final String DATABASE_NAME = "MusicPlayer";

    private static final String TABLE_NAME1 = "playlists";
    private static final String COL11 = "id";
    private static final String COL12 = "name";

    private static final String TABLE_NAME2 = "song_playlist";
    private static final String COL21 = "song";
    private static final String COL22 = "playlist";

    private static final String TABLE_NAME3 = "favourites";
    private static final String COL31 = "song_id";

    private static final String CREATE_PLAYLISTS_TABLE = "CREATE TABLE " + TABLE_NAME1 + " (" + COL11 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL12 + " TEXT);";
    private static final String CREATE_SONGS_PLAYLIST_TABLE = "CREATE TABLE " + TABLE_NAME2 + " (" + COL21 + " INTEGER, " + COL22 + " INTEGER, " + "FOREIGN KEY(" + COL22 + ") REFERENCES " + TABLE_NAME1 + "(" + COL11 + "), " + "PRIMARY KEY(" + COL21 + ", " + COL22 + "));";
    private static final String CREATE_FAVOURITES_TABLE = "CREATE TABLE " + TABLE_NAME3 + " (" + COL31 + "INTEGER PRIMARY KEY);";


    /**
     * Creates an instance of DatabaseHelper if none exist, or return one if one exists
     * @return a DatabaseHelper instance
     */
    public static DatabaseHelper getInstance() {
        if(databaseHelper == null) {
            databaseHelper = new DatabaseHelper(MainActivity.mainActivity);
        }

        return databaseHelper;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        databaseHelper = this;
    }

    /**
     * Creates the tables
     * @param db the writable database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("mydatabaseherlper", "ahahahahaha!");
        db.execSQL(CREATE_PLAYLISTS_TABLE);
        db.execSQL(CREATE_SONGS_PLAYLIST_TABLE);
        db.execSQL(CREATE_FAVOURITES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME1);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME2);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME3);

        onCreate(db);
    }

    /**
     * Adds a new Playlist object in the database
     * @param name the name of the playlist
     * @return the id of the newly created playlist
     */
    public int addPlaylist(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(COL12, name);

        db.insert(TABLE_NAME1, null, c);

        Cursor cu = db.rawQuery("SELECT last_insert_rowid()", null);

        if(cu != null)
            cu.moveToFirst();

        int ret = cu.getInt(0);

        cu.close();

        return ret;
    }

    /**
     * Links a song to a playlist
     * @param song_id the song to add to the playlist
     * @param playlist_id the playlist in which the song will be added
     * @return true if the transaction worked, else false
     */
    public boolean linkSongPlaylist(int song_id, int playlist_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(COL21, song_id);
        c.put(COL22, playlist_id);

        long result = db.insert(TABLE_NAME2, null, c);

        return (result != -1);
    }

    /**
     * Get all the songs that belongs to a given playlist
     * @param playlist the playlist which will be queried
     * @return a list of songs
     */
    public List<Song> getSongsForPlaylist(Playlist playlist) {
        List<Song> songs = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + COL21 + " FROM " + TABLE_NAME2 + " WHERE " + COL22 + " = " + playlist.getId() + ";";

        Cursor c = db.rawQuery(query, null);

        if(c.moveToFirst()) {
            do {
                songs.add(Song.getSongById(c.getInt(0)));
            } while(c.moveToNext());
        }

        c.close();

        return songs;
    }

    /**
     * Returns the number of songs in a given playlist
     * @param playlist the playlist wich we want to count the songs of
     * @return the total number
     */
    public int getSongsCountForPlaylist(Playlist playlist) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT count(*) FROM " + TABLE_NAME2 + " WHERE " + COL22 + " = " + playlist.getId() + ";";

        Cursor c = db.rawQuery(query, null);

        if(c != null)
            c.moveToFirst();

        int ret = c.getInt(0);
        c.close();

        return ret;
    }

    /*
    /**
     * Get all the playlists the song given belongs
     * @param song
     * @return a list of playlists
     */
    /*public List<Playlist> getPlaylistsForSong(Song song) {
        List<Playlist> playlists = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + COL22 + " FROM " + TABLE_NAME2 + " WHERE " + COL21 + " = " + song.getId() + ";";

        Cursor c = db.rawQuery(query, null);

        if(c.moveToFirst()) {
            do {
                playlists.add(Playlist.getPlaylistById(c.getInt(0)));
            } while(c.moveToNext());
        }

        c.close();

        return playlists;
    }*/

    /**
     * Removes a playlist from the database
     * @param playlist the playlist to remove
     */
    public void deletePlaylist(int playlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        //1: Suppression dans songs_playlist
        String query = "DELETE FROM " + TABLE_NAME2 + " WHERE " + COL22 + " = " + playlist;
        db.execSQL(query);

        //2: Suppression dans playlists
        query = "DELETE FROM " + TABLE_NAME1 + " WHERE " + COL11 + " = " + playlist;
        db.execSQL(query);
    }

    /**
     * Adds the song in the favourites table
     * @param song_id the id of the song to add
     */
    private void addFavourite(int song_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(COL12, song_id);

        db.insert(TABLE_NAME3, null, c);
    }

    /**
     * Remove the song in the favourites table
     * @param song_id the id of song to remove
     */
    private void removeFavourite(int song_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "DELETE FROM " + TABLE_NAME3 + " WHERE " + COL31 + " = " + song_id;
        db.execSQL(query);
    }

    /**
     * Queries the database whether the given song is in the favourites table or not
     * @param song_id the id of the song to query
     * @return true if it is in the favourites table, false else
     */
    public boolean isFavourite(int song_id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT " + COL31 + " FROM " + TABLE_NAME3 + " WHERE " + COL31 + " = " + song_id;

        Cursor c = db.rawQuery(query, null);

        boolean ret = c.getCount() > 0;
        c.close();

        return ret;
    }

    /**
     * If a song is not in the favourites table, add it, remove it if it is in the table
     * @param song_id the id of the song to toggle
     */
    public void toggleFavourite(int song_id) {
        if(isFavourite(song_id)) {
            removeFavourite(song_id);
        } else {
            addFavourite(song_id);
        }
    }
}
