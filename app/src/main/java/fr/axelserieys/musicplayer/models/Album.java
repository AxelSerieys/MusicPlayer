package fr.axelserieys.musicplayer.models;

public class Album {
    private String name;
    private int id;
    private int year;
    private static int currentId = 0;

    public Album(String name) {
        this.name = name;
        this.id = currentId;
        currentId++;
    }

    public String getName() { return this.name; }
    public int getId() { return this.id; }
}
