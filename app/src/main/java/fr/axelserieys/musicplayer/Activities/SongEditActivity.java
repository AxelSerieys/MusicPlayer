package fr.axelserieys.musicplayer.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.TagException;
import org.jaudiotagger.tag.id3.ID3v1Tag;

import java.io.File;
import java.io.IOException;

import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.models.Song;

public class SongEditActivity extends AppCompatActivity {

    private Song song;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_edit);

        int id = getIntent().getExtras().getInt("song");
        song = Song.getSongById(id);

        ((TextView) findViewById(R.id.page_title)).setText(song.getTitle());
        ((TextView) findViewById(R.id.et_title)).setText(song.getTitle());

        try {
            MP3File mp3File = new MP3File(song.getPath());
            ID3v1Tag id3v1Tag;

            if(mp3File.hasID3v1Tag()) {
                id3v1Tag = mp3File.getID3v1Tag();
            } else {
                id3v1Tag = new ID3v1Tag();
                mp3File.setID3v1Tag(id3v1Tag);
            }

            id3v1Tag.setTitle("Les amoureux");

            mp3File.save();







        } catch (IOException | TagException | ReadOnlyFileException | CannotReadException | InvalidAudioFrameException  e) {
            Log.d("axeldebug", e.getMessage());
        }




        /*try {
            MusicMetadataSet metadata = new MyID3().read(new File(song.getPath()));

            MusicMetadata mm = new MusicMetadata("name");
            String old = mm.getSongTitle();
            mm.setSongTitle("les amoureux");

            new MyID3().write(new File(song.getPath()), new File(song.getPath()), metadata, mm); //File dir, File dst, MusicMetadataSet set,  MusicMetadata values

            Log.d("axelmetadata", "coucou");

            Log.d("axelmetadata", mm + "|" + mm.getSongTitle());

            Log.d("axelmetadata", metadata.toString());



        } catch (IOException e) {
            e.printStackTrace();
        } catch (ID3WriteException e) {
            e.printStackTrace();
        }*/
    }
}
