package fr.axelserieys.musicplayer.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import fr.axelserieys.musicplayer.Player.MusicPlayer;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.database.DatabaseHelper;

public class PlayerActivity extends AppCompatActivity {
    public static SeekBar playerSeekbar = null;
    public static TextView currentTimeTv = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        updateFavoriteIcon();

        ((TextView) findViewById(R.id.song_title)).setText(MusicPlayer.currentSong.getTitle());
        ((TextView) findViewById(R.id.song_artist)).setText(MusicPlayer.currentSong.getArtist().getName());

        ((ImageView) findViewById(R.id.song_icon)).setImageBitmap(MusicPlayer.currentSong.getIcon());

        if(MusicPlayer.isPlaying) {
            ((ImageView) findViewById(R.id.pause_button)).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.play_button)).setVisibility(View.GONE);
        } else {
            ((ImageView) findViewById(R.id.play_button)).setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.pause_button)).setVisibility(View.GONE);
        }

        currentTimeTv = findViewById(R.id.currentTime);
        playerSeekbar = findViewById(R.id.playerSeekbar);
        playerSeekbar.setMax(MusicPlayer.player.getDuration());
        ((TextView) findViewById(R.id.totalDuration)).setText(millisToString(MusicPlayer.player.getDuration()));
    }

    public static String millisToString(int millis) {
        StringBuilder sb = new StringBuilder();
        double hours = millis / 1000.0 / 60.0 / 60.0;
        int exactHours = (int) hours;

        double minutes = (hours - exactHours) * 60.0;
        int exactMinutes = (int) minutes;

        double seconds = (minutes - exactMinutes) * 60.0;
        int exactSeconds = (int) seconds;

        if(exactHours != 0) { //Doesn't display the hours is exactHours = 0.
            sb.append(exactHours);
            sb.append(":");
        }
        if(exactMinutes > 0 && exactMinutes < 10) { //Adds 0 to the minutes if 0 < exactMinutes < 10.
            sb.append("0");
        }
        sb.append(exactMinutes);
        sb.append(":");
        if(exactSeconds > 0 && exactSeconds < 10) { //Adds 0
            sb.append("0");
        }
        sb.append(exactSeconds);
        if(exactSeconds == 0) { //Adds 0 if 0 = exactSeconds
            sb.append("0");
        }

        return sb.toString();
    }

    public static void updateCurrentTime(int millis) {
        if(currentTimeTv != null) currentTimeTv.setText(millisToString(millis));
    }

    public void onPlay(View view) {
        MusicPlayer.play();

        ((ImageView) findViewById(R.id.pause_button)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.play_button)).setVisibility(View.GONE);
    }

    public void onPause(View view) {
        MusicPlayer.pause();

        ((ImageView) findViewById(R.id.play_button)).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.pause_button)).setVisibility(View.GONE);
    }

    public void forward(View view) {
        MusicPlayer.forward();
    }

    public void backward(View view) {
        MusicPlayer.backward();
    }

    public void toggleFavourite(View view) {
        DatabaseHelper.getInstance().toggleFavourite(MusicPlayer.currentSong.getId());

        updateFavoriteIcon();
    }

    public void updateFavoriteIcon() {
        if(DatabaseHelper.getInstance().isFavourite(MusicPlayer.currentSong.getId())) {
            ((ImageView) findViewById(R.id.favourite)).setImageDrawable(getDrawable(R.drawable.ic_heart_blue));
        } else {
            ((ImageView) findViewById(R.id.favourite)).setImageDrawable(getDrawable(R.drawable.ic_heart_empty));
        }
    }
}
