package fr.axelserieys.musicplayer.adapters;

import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.Player.MusicPlayer;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.controllers.PlaylistActivity;
import fr.axelserieys.musicplayer.database.DatabaseHelper;
import fr.axelserieys.musicplayer.fragments.FragmentArtists;
import fr.axelserieys.musicplayer.fragments.FragmentSongs;
import fr.axelserieys.musicplayer.models.Album;
import fr.axelserieys.musicplayer.models.Artist;
import fr.axelserieys.musicplayer.models.Playlist;
import fr.axelserieys.musicplayer.models.Song;

public class SongAdapter extends BaseAdapter implements View.OnClickListener, View.OnLongClickListener  {

    private Context context;
    private Song currentSong;
    private LayoutInflater inflater;

    private static SongAdapter songAdapter = null;

    private static boolean useSongMasterArray;


    public SongAdapter(Context context, boolean useSongMasterArray) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.useSongMasterArray = useSongMasterArray;
        songAdapter = this;
    }

    @Override
    public int getCount() {
        return (useSongMasterArray) ? MainActivity.master_songs.size() : MainActivity.songs.size();
    }

    @Override
    public Song getItem(int position) {
        return (useSongMasterArray) ? MainActivity.master_songs.get(position) : MainActivity.songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.adapter_song, null);

        currentSong = getItem(i);
        String songName = currentSong.getTitle();
        Artist songArtist = currentSong.getArtist();
        String songId = String.valueOf(currentSong.getId());

        MainActivity.searchInput.setOnEditorActionListener(new EditText.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String q = MainActivity.searchInput.getText().toString();
                    FragmentSongs.getMusic(q);
                    FragmentArtists.getArtists(q);
                    notifyDataSetChanged();
                }
                return false;
            }
        });

        TextView songNameView = view.findViewById(R.id.song_name);
        songNameView.setText(songName);

        TextView songArtistView = view.findViewById(R.id.song_artist);
        songArtistView.setText(songArtist.getName());

        TextView songIdView = view.findViewById(R.id.song_id);
        songIdView.setText(songId);

        view.setOnLongClickListener(this);
        view.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        if(((ListView) v.getParent()).getId() == R.id.addPlaylistSongsList) {
            PlaylistActivity.playlistActivity.onClick(v);
        } else {
            Song song = FragmentSongs.findSongById(Integer.valueOf(((TextView) v.findViewById(R.id.song_id)).getText().toString()), false);

            MainActivity.getInstance().play(null);
            FragmentSongs.getInstance().updateBottomBar(song);
            MusicPlayer.playSong(context, song);

            TextView songNameView = v.findViewById(R.id.song_name);
            TextView songArtistView = v.findViewById(R.id.song_artist);

            MusicPlayer.colorPreviousSongInGrey();
            MusicPlayer.colorSongPlaying(new TextView[]{songNameView, songArtistView});
        }
    }

    public static void showMusicsForArtist(Artist artist) {
        MainActivity.songs = Song.getSongsForArtist(artist.getId());

        songAdapter.notifyDataSetChanged();
    }

    public static void showMusicsForAlbum(Album album) {
        MainActivity.songs = Song.getSongsForAlbum(album.getId());

        songAdapter.notifyDataSetChanged();
    }

    public static void showMusicsForPlaylist(Playlist playlist) {
        MainActivity.songs = DatabaseHelper.getInstance().getSongsForPlaylist(playlist);

        songAdapter.notifyDataSetChanged();
    }


    @Override
    public boolean onLongClick(View v) {
        MainActivity.mainActivity.openContextMenu(v);
        return true;
    }
}
