package fr.axelserieys.musicplayer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.fragments.FragmentArtists;
import fr.axelserieys.musicplayer.models.Artist;
import fr.axelserieys.musicplayer.models.Song;

public class ArtistAdapter extends BaseAdapter implements View.OnClickListener {

    private static Context mContext;
    private LayoutInflater inflater;
    private Artist currentArtist;

    public ArtistAdapter(Context context) {
        this.mContext = context;

        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() { return MainActivity.artists.size(); }

    @Override
    public Artist getItem(int position) { return MainActivity.artists.get(position); }

    @Override
    public long getItemId(int position) { return getItem(position).getId(); }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.adapter_artist, null);

        currentArtist = getItem(position);
        String currentName = currentArtist.getName();
        int currentCount = Song.getSongsCountForArtist(currentArtist.getId());

        TextView artistNameView = view.findViewById(R.id.artist_name);
        artistNameView.setText(currentName);

        TextView artistCountView = view.findViewById(R.id.artist_count);
        artistCountView.setText(""+currentCount);

        TextView artistIdView = view.findViewById(R.id.artist_id);
        artistIdView.setText(""+currentArtist.getId());

        view.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        Artist artist = FragmentArtists.findArtistById(Integer.valueOf(((TextView) v.findViewById(R.id.artist_id)).getText().toString()));

        SongAdapter.showMusicsForArtist(artist);

        MainActivity.tabs.getTabAt(0).select();
    }
}
