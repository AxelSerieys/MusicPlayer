package fr.axelserieys.musicplayer.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.adapters.AlbumAdapter;
import fr.axelserieys.musicplayer.models.Album;
import fr.axelserieys.musicplayer.models.Song;

public class FragmentAlbums extends Fragment {

    private static FragmentAlbums fragmentAlbums;
    private static Context context;
    private static ListView listView;

    public static FragmentAlbums newInstance(Context context) {
        if(fragmentAlbums == null) {
            fragmentAlbums = new FragmentAlbums();
            FragmentAlbums.context = context;
        }

        fragmentAlbums.getAlbums("");
        Collections.sort(MainActivity.albums, new AlbumComparator());

        return fragmentAlbums;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_albums, container, false);

        listView = rootView.findViewById(R.id.albums_list);
        listView.setAdapter(new AlbumAdapter(context));

        return rootView;
    }

    public void getAlbums(String query) {
        MainActivity.albums.clear();

        for(Song song : MainActivity.master_songs) {
            boolean found = false;
            for(Album album : MainActivity.albums)
                if(album.getId() == song.getAlbum().getId())
                    found = true;

            if(found == false && song.getAlbum().getName().toLowerCase().contains(query.toLowerCase())) {
                MainActivity.albums.add(song.getAlbum());
            }
        }

        if(MainActivity.albums.size() == 0) {
            Toast.makeText(context, "Sorry, no album was found with the current search query", Toast.LENGTH_LONG).show();
        }
    }

    public static Album findAlbumById(int id) {
        for(Album album : MainActivity.albums)
            if(album.getId() == id)
                return album;
        return null;
    }

}

class AlbumComparator implements Comparator<Album> {

    @Override
    public int compare(Album left, Album right) {
        return left.getName().compareTo(right.getName());
    }
}
