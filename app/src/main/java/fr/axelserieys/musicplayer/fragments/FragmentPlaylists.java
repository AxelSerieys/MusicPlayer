package fr.axelserieys.musicplayer.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.adapters.PlaylistAdapter;
import fr.axelserieys.musicplayer.models.Playlist;

public class FragmentPlaylists extends Fragment {
    private static FragmentPlaylists fragmentPlaylists = null;
    private static Context context;
    private static ListView listView;

    public static FragmentPlaylists newInstance(Context context) {
        if(fragmentPlaylists == null) {
            fragmentPlaylists = new FragmentPlaylists();
            FragmentPlaylists.context = context;
        }

        initPlaylists();
        //Collections.sort(MainActivity.artists, new ArtistComparator());

        return fragmentPlaylists;
    }

    public static FragmentPlaylists getInstance() {
        return fragmentPlaylists;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_playlists, container, false);

        listView = rootView.findViewById(R.id.playlists_list);
        listView.setAdapter(new PlaylistAdapter(context));

        return rootView;
    }

    public static void initPlaylists() {
        MainActivity.playlists = Playlist.getAllPlaylists(context);
    }
}
