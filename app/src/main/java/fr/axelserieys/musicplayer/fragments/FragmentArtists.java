package fr.axelserieys.musicplayer.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;

import fr.axelserieys.musicplayer.Activities.MainActivity;
import fr.axelserieys.musicplayer.R;
import fr.axelserieys.musicplayer.adapters.ArtistAdapter;
import fr.axelserieys.musicplayer.models.Artist;
import fr.axelserieys.musicplayer.models.Song;

public class FragmentArtists extends Fragment {

    private static FragmentArtists fragmentArtists = null;
    private static Context context;
    private static ListView listView;

    public static FragmentArtists newInstance(Context context) {
        if(fragmentArtists == null) {
            fragmentArtists = new FragmentArtists();
            FragmentArtists.context = context;
        }

        fragmentArtists.getArtists("");
        Collections.sort(MainActivity.artists, new ArtistComparator());

        return fragmentArtists;
    }

    public static FragmentArtists getInstance() {
        return fragmentArtists;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_artists, container, false);

        listView = rootView.findViewById(R.id.artists_list);
        listView.setAdapter(new ArtistAdapter(context));

        return rootView;
    }

    public static void getArtists(String query) {
        MainActivity.artists.clear();

        for(Song song : MainActivity.master_songs) {
            boolean found = false;
            for(Artist artist : MainActivity.artists)
                if(artist.getId() == song.getArtist().getId())
                    found = true;

            if(found == false && song.getArtist().getName().toLowerCase().contains(query.toLowerCase())) {
                MainActivity.artists.add(song.getArtist());
            }
        }

        if(MainActivity.artists.size() == 0) {
            Toast.makeText(context, "Sorry, no artist was found with the current search query", Toast.LENGTH_LONG).show();
        }
    }

    public static Artist findArtistById(int id) {
        for(Artist artist : MainActivity.artists)
            if(artist.getId() == id)
                return artist;
        return null;
    }

}

class ArtistComparator implements Comparator<Artist> {

    @Override
    public int compare(Artist left, Artist right) {
        return left.getName().compareTo(right.getName());
    }
}
